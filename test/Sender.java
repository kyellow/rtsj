/*
 * Sender.java
 *
 * Created on June 14, 2012, 11:10 AM 
 */
package rtsj;

import java.io.IOException;
import jpcap.JpcapCaptor;
import jpcap.NetworkInterface; 
import jpcap.packet.EthernetPacket;
import jpcap.packet.Packet;

/**
 *
 * @author kyellow
 */
public class Sender{

    public static void main(String[] args) throws IOException,InterruptedException{ 
	
	NetworkInterface[] device = JpcapCaptor.getDeviceList();
	JpcapSender at = JpcapSender.openDevice(device[0]);
	EthernetPacket ether = new EthernetPacket();
	Packet paket = new Packet();
	MacAdr mac = new MacAdr("00:08:74:D3:A1:19"); //alıcı bilgisayarın mac adresi
	ether.src_mac = devices[0].mac_address;		
	ether.dst_mac=mac.asByteArray();		
	paket.datalink=ether;
	paket.data=("veri").getBytes();

	for(int i=0; i<100; i++)
	{
		at.sendPacket(paket);
    	}

	at.close();
    }
}
