/*
 * Client.java
 *
 * Created on June 17, 2012, 1:23 PM 
 */
package rtsj;

import java.io.IOException;
import jpcap.JpcapCaptor;
import jpcap.JpcapSender;
import jpcap.NetworkInterface; 
import jpcap.packet.EthernetPacket;
import jpcap.packet.Packet;
import javax.realtime.*;

/**
 *
 * @author kyellow
 */
public class Client {

    public static void main(String[] args) throws IOException, InterruptedException { 

	PriorityScheduler ps = (PriorityScheduler)Scheduler.getDefaultScheduler();
        PriorityParameters pri = new PriorityParameters(ps.getMaxPriority());
        //schedule real time thread at every 2 milisecond
        PeriodicParameters pp = new PeriodicParameters(
                new RelativeTime(0,0),  	// start
                new RelativeTime(2, 0),   	// period
                new RelativeTime(1,0),  	// cost
                new RelativeTime(1,0),null,null); // deadline
        Sender s = new Sender(pri,pp);
        s.start();
        
        //wait real time thread to terminate
        try {
            s.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}

public class Sender extends RealtimeThread { 	//RealtimeThread sınıfından türetilmiş Capture sınıfı

    
    private boolean noProblems = true;
    private JpcapCaptor captor;
    private JpcapSender sendor;
    private Packet paket;
    private PacketPrinter print;
        
    public Sender(PriorityParameters pri, PeriodicParameters pp)
    {
        super(pri,pp)
	this.init();
    }

    private void init()
    {
	MacAddress smac = new MacAddress("00:26:22:39:7D:C8");		// server mac 	  
	EthernetPacket ethernetPaket = new EthernetPacket(); 
	ethernetPaket.dst_mac = smac.asByteArray();      			
        ethernetPaket.src_mac = JpcapCaptor.getDeviceList()[0].mac_address;     
        ethernetPaket.frametype = 0;      			   // #frametype (Ether Type : https://en.wikipedia.org/wiki/EtherType)

	paket = new Packet();
        paket.data = new byte[]{(byte) 03, (byte) 13}; 	 // #veri 
        paket.datalink = ethernetPaket;      		 // datalink

	print = new PacketPrinter(sendor,paket,mod);
	
	try {
            //Capturemak için sürücüyü aç zaman aşımını kaldır '0'
            captor = JpcapCaptor.openDevice(JpcapCaptor.getDeviceList()[0], 2000, false, 0);
            sendor = JpcapSender.openDevice(JpcapSender.getDeviceList()[0]);
        } catch (Exception ex) {
            System.out.println(" Hata :" + ex.getMessage());
	    noProblems = false;
        }
    }

    public void run() {
        
        while (noProblems) {
            captor.loopPacket(-1, p); 		//sürekli paket yakala ve  yakalanan her bir paketi p nesnesine geçir.
            noProblems = waitForNextPeriod();
        }
    }
    
    private void deinit()
    {
	captor.close();
	sendor.close();
    }
}
