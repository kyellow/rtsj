/*
 * Server.java
 *
 * Created on June 17, 2012, 1:23 PM 
 */
package rtsj;

import java.io.IOException;
import jpcap.JpcapCaptor;
import jpcap.JpcapSender;
import jpcap.NetworkInterface; 
import jpcap.packet.EthernetPacket;
import jpcap.packet.Packet;
import javax.realtime.*;

/**
 *
 * @author ktest
 */
public class Server {

    public static void main(String[] args) throws IOException, InterruptedException {
	
	PriorityScheduler ps = (PriorityScheduler)Scheduler.getDefaultScheduler();
        PriorityParameters pri = new PriorityParameters(ps.getMaxPriority());
        //schedule real time thread at every 2 milisecond
        PeriodicParameters pp = new PeriodicParameters(
                new RelativeTime(0,0),     // start
                new RelativeTime(2, 0),    // period
                new RelativeTime(1,0),     // cost
                new RelativeTime(1,0),null,null); // deadline
        Capture c = new Capture(pri,pp);
        c.start();
        
        //wait real time thread to terminate
        try {
            c.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}

class Capture extends RealtimeThread { 	//RealtimeThread sınıfından türetilmiş Capture sınıfı

    private boolean noProblems = true;
    private JpcapCaptor captor;
    private JpcapSender sendor;
    private Packet paket;
    private PacketPrinter print;
    
    public Capture(PriorityParameters pri, PeriodicParameters pp)
    {
        super(pri,pp)
	this.init();
    }

    private void init()
    {
	MacAddress cmac = new MacAddress("00:08:74:D3:A1:19");		// client mac 	  
	EthernetPacket ethernetPaket = new EthernetPacket(); 
	ethernetPaket.dst_mac = cmac.asByteArray();      			
        ethernetPaket.src_mac = JpcapCaptor.getDeviceList()[0].mac_address;     
        ethernetPaket.frametype = 0;      			   // #frametype  (Ether Type : https://en.wikipedia.org/wiki/EtherType)

	paket = new Packet();
        paket.data = new byte[]{(byte) 04, (byte) 14}; 	 // #veri 
        paket.datalink = ethernetPaket;      		 // datalink

	print = new PacketPrinter(sendor,paket,mod);
	
	try {
            //Capturemak için sürücüyü aç zaman aşımını kaldır '0'
            captor = JpcapCaptor.openDevice(JpcapCaptor.getDeviceList()[0], 2000, false, 0);
            sendor = JpcapSender.openDevice(JpcapSender.getDeviceList()[0]);
        } catch (Exception ex) {
            System.out.println(" Hata :" + ex.getMessage());
	    noProblems = false;
        }
    }

    public void run() {
        
        while (noProblems) {
            captor.loopPacket(-1, p); 		//sürekli paket yakala ve  yakalanan her bir paketi p nesnesine geçir.
            noProblems = waitForNextPeriod();
        }
    }
}
