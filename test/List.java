/*
 * List.java
 *
 * Created on June 13, 2012, 10:36 AM 
 */
package rtsj;

import java.io.IOException;
import jpcap.JpcapCaptor;
import jpcap.NetworkInterface; 

/**
 *
 * @author kyellow
 */
public class List{

    public static void main(String[] args) throws IOException,InterruptedException{ 
	
	NetworkInterface[] device = JpcapCaptor.getDeviceList();
	
	for(int i=0; i<devices.length; i++)
	{
		System.out.println(i+" : "+devices[i].name +" ( "+devices[i].description+" )");
		System.out.println(" Datalink : "+devices[i].datalink_name+"( "+devices[i].datalink_description+")");

		System.out.println(" Mac Address : ");

		for(byte a : devices[i].mac_address) //mac adres eldesi
			System.out.print(Integer.toHexString(a&0xff)+":");

		//mac adres hex dönüşümü
		for(NetworkInterfaceAddress adr : devices[i].addresses)
			System.out.print(" Adress : "+adr.address +" "+adr.subnet+" "+adr.broadcast);
	
		System.out.println();
	}
    }
}
