# rtsj
Real-Time Communication with Ethernet Packets.
<img src="https://gitlab.com/kyellow/rtsj/raw/master/ethernetpacket.png" width="608" height="71"/>

## Description
rtsj was developed the specialized two network cards to communicate with ethernet packets in real-time.
<img src="https://gitlab.com/kyellow/rtsj/raw/master/ab.png" width="241" height="273"/>

## Prerequisites

 * RT Linux
 * Real-Time Java VM

|                   |Application    |Latency/Jitter               |
| ----------------- |:-------------:| ---------------------------:|
|Standard O/S       |Non Real Time  |100 micro to 100 milliseconds|
|Standard Linux     |Soft Real Time |1 millisecond                |
|IEEE 1003.1d Linux |Hard Real Time |10 to 100 microseconds       |
|Real-Time Linux    |Hard Real Time |1 to 10 microseconds         |
|RTOS Kernels       |Hard Real Time |1 to 10 microseconds         |
