/*
 * Server.java
 *
 * Created on June 17, 2012, 3:14 PM 
 */
package rtsj;

/**
 *
 * @author kyellow
 */
public class MacAddress {

    private final static int NUM_OF_FIELDS = 6;
    private boolean _longSet = false;
    private long _longValue = 0;
    private byte[] myBytes = null;	//MacByte

    public MacAddress(byte[] theBytes) //
    {
        myBytes = theBytes;
        if (myBytes.length < NUM_OF_FIELDS) {
            byte[] tmp = new byte[NUM_OF_FIELDS];
            System.arraycopy(myBytes, 0, tmp, NUM_OF_FIELDS - myBytes.length, myBytes.length);
            myBytes = tmp;
        }
    }

    public MacAddress(String theMac) //
    {
        String[] fields = theMac.split(":");

        if (fields.length > NUM_OF_FIELDS) {
            String[] tmp = new String[NUM_OF_FIELDS];
            System.arraycopy(fields, 0, tmp, 0, NUM_OF_FIELDS);
            fields = tmp;
        }
        myBytes = new byte[NUM_OF_FIELDS];
        for (int i = NUM_OF_FIELDS - fields.length; i < NUM_OF_FIELDS; i++) {
            myBytes[i] = Integer.decode("0x" + fields[i - (NUM_OF_FIELDS - fields.length)]).byteValue();
        }
    }

    public MacAddress(long theMacAsInt) //
    {
        long sum = (theMacAsInt > 0) ? theMacAsInt : 0;
        myBytes = new byte[NUM_OF_FIELDS];
        int counter = NUM_OF_FIELDS - 1;
        while (sum > 0) {
            myBytes[counter] = (byte) (sum % 256);
            sum = sum / 256;
            counter--;
            if (counter < 0) {
                break;
            }
        }
    }

    public byte[] asByteArray() //return byte mac
    {
        return myBytes;
    }

    public String toString() //byte to string
    {
        long sum = 0;
        for (int i = 0; i < myBytes.length; i++) {
            sum = sum * 256 + (myBytes[i] & 0xff);
        }
        String toReturn = Long.toHexString(sum);
        for (int i = toReturn.length(); i < 12; i++) {
            toReturn = "0" + toReturn;
        }
        return toReturn;
    }

    public String toReadbleString() //Mac to String
    {
        char[] tmpArr = new char[NUM_OF_FIELDS * 3];
        int idx = 0;
        for (int i = 0; i < myBytes.length; i++) {
            if (i > 0) {
                tmpArr[idx++] = ':';
            }
            int num = 0xff & myBytes[i];

            int second1 = (num & 0x0f);
            int first1 = ((num & 0xf0) >> 4);

            char second = (char) ((second1 < 10) ? '0' + second1 : 'A' + second1 - 10);
            char first = (char) ((first1 < 10) ? '0' + first1 : 'A' + first1 - 10);

            tmpArr[idx++] = first;
            tmpArr[idx++] = second;
        }
        return new String(tmpArr, 0, idx);
    }

    public long getAsLong() {
        if (_longSet == true) {
            return _longValue;
        }

        long sum = 0;
        for (int i = 0; i < myBytes.length; i++) {
            sum = sum * 256 + (myBytes[i] & 0xff);

        }
        _longValue = sum;
        _longSet = true;
        return sum;
    }

    public static MacAddress incMAC(MacAddress theMac) 
    {
        long val = theMac.getAsLong();
        return new MacAddress(++val);
    }

    public static MacAddress decMAC(MacAddress theMAC) 
    {
        long val = theMAC.getAsLong();
        return new MacAddress(++val);
    }

    public boolean equals(Object theObj) {
        if (theObj == null || !(theObj instanceof MacAddress)) {
            return false;
        }
        return getAsLong() == (((MacAddress) theObj).getAsLong());
    }

    @Override
    public int hashCode() {
        return (int) getAsLong();
    }

}
