/*
 * Packet.java
 *
 * Created on June 13, 2012, 2:43 PM 
 */
package rtsj;

import java.io.IOException;
import jpcap.packet.EthernetPacket;
import jpcap.packet.Packet;

/**
 *
 * @author kyellow
 */
public class Packet{

    public static void main(String[] args){ 
	
	Packet paket = new Packet(); //boş paket
	EthernetPacket ether = new EthernetPacket(); //ethernet paketi

	//kaynak ve hedef mac adresleri
	ether.src_mac = new byte[]{(byte)2,(byte)13,(byte)116,(byte)102,(byte)26,(byte)95};
	ether.dst_mac = new byte[]{(byte)0,(byte)8,(byte)116,(byte)211,(byte)161,(byte)25};

	ether.frametype = 0; 
	paket.datalink = ether; //ethernet paketini boş pakete bağlanması
	paket.data = ("veri burada").getBytes(); //pakete veri eklenmesi
	String str = new String(paket.data,Charset.forName("US-ASCII")); //karakter kodlaması
	System.out.println("Paket ->"+str);
    }
}
