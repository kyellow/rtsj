/*
 * PacketPrinter.java
 *
 * Created on June 18, 2012, 11:46 AM 
 */
package rtsj;

import jpcap.JpcapSender; 
import jpcap.NetworkInterface; 
import jpcap.PacketReceiver; 
import jpcap.packet.Packet; 

/**
 *
 * @author kyellow
 */
public class PacketPrinter implements PacketReceiver { 
    
    private static final byte data[] = new byte[]{04, 14, 03, 13};
    private int index;
    private JpcapSender reply;
    private Packet paket;

    public PacketPrinter(JpcapSender s, Packet p, boolean isServer) { 
	this.reply = s;
	this.paket = p;
	
	if( isServer )
		this.index = 2;
	else
		this.index = 0;    
    } 

    public void sendPacket() { 
        reply.sendPacket(paket); 
    } 

    public void receivePacket(Packet pak) { 

        if (pak != null) { 
            if (pak.data[0] == this.data[index] && pak.data[1] == this.data[index + 1]) { 
                this.sendPacket(); 
            } 
        } 
    } 
}
