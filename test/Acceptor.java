/*
 * Acceptor.java
 *
 * Created on June 14, 2012, 11:16 AM 
 */
package rtsj;

import java.io.IOException;
import jpcap.JpcapCaptor;
import jpcap.NetworkInterface; 
import jpcap.packet.Packet;

/**
 *
 * @author kyellow
 */
public class Acceptor{

    public static void main(String[] args) throws IOException{ 
	
	NetworkInterface[] device = JpcapCaptor.getDeviceList();
	JpcapCaptor al = JpcapCaptor.openDevice(device[0],65535,false,0);

	for(int i=0; i<100; i++)
	{
		Packet paket = al.getPacket();
		String str = new String(paket.data,Charset.forName("US-ASCII")); //karakter kodlaması
		System.out.println(i+“. ->”+str);
    	}

	al.close();
    }
}
